<?php

namespace Services;

abstract class ApiService
{
    protected string $endpoint;

    public function request($method, $path, $data = [])
    {
        return $this->getRequest($method, $path, $data)->json();
    }

    public function getRequest($method, $path, $data = [])
    {
        return \Http::acceptJson()->withHeaders(
            [
                'Authorization' => 'Bearer ' . request()->cookie('jwt')
            ]
        )
            ->$method("{$this->endpoint}/{$path}", $data);
    }

    /**
     * @throws \Exception
     */
    public function post($path, $data = [])
    {
        return $this->request('post', $path, $data);
    }

    /**
     * @throws \Exception
     */
    public function get($path)
    {
        return $this->request('get', $path);
    }

    /**
     * @throws \Exception
     */
    public function put($path, $data)
    {
        return $this->request('put', $path, $data);
    }

    /**
     * @throws \Exception
     */
    public function delete($path)
    {
        return $this->request('delete', $path);
    }
}
